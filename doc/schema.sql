CREATE SCHEMA `course_web`
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_bin;
USE course_web;

CREATE TABLE course (
  no        INT PRIMARY KEY AUTO_INCREMENT
  COMMENT '课程编号',
  name      VARCHAR(50) COMMENT '课程名称',
  tno       INT COMMENT '开课教师编号',
  startweek INT COMMENT '第几周第一节课',
  endweek   INT COMMENT '第几周最后一节课',
  dayofweek INT COMMENT '星期几，每周都只有一节课',
  lesson    INT COMMENT '一天中的第几节, 1代表上午第1节课，
                        4代表下午最后一节课，5代表晚上',
  classroom VARCHAR(50) COMMENT '教室名称，如D101',
  type      INT COMMENT '0: 非实验课，1：实验课',
  state     INT COMMENT '0：还未通过审核，1：审核通过，2：审核不通过，
                         3：已结课'
) 
  AUTO_INCREMENT = 100001
  DEFAULT CHARSET utf8, ENGINE INNODB
  COLLATE = utf8_bin
  COMMENT = '课程';

CREATE TABLE teacher (
  no     INT PRIMARY KEY AUTO_INCREMENT
  COMMENT '教师编号',
  name   VARCHAR(50) COMMENT '教师名称',
  age    INT COMMENT '教师年龄',
  gender VARCHAR(10) COMMENT '教师性别',
  phone  VARCHAR(20) COMMENT '教师电话',
  office VARCHAR(20) COMMENT '教师办公位置'
)
  AUTO_INCREMENT = 200001
  DEFAULT CHARSET utf8, ENGINE INNODB
  COLLATE = utf8_bin
  COMMENT = '教师';
CREATE TABLE student (
  no        INT PRIMARY KEY AUTO_INCREMENT
  COMMENT '学生编号',
  name      VARCHAR(50) COMMENT '学生名称',
  age       INT COMMENT '教师年龄',
  gender    VARCHAR(10) COMMENT '教师性别',
  phone     VARCHAR(20) COMMENT '联系电话',
  classname VARCHAR(20) COMMENT '班级名称',
  apartment VARCHAR(20) COMMENT '宿舍'
)
  AUTO_INCREMENT = 300001
  DEFAULT CHARSET utf8, ENGINE INNODB
  COLLATE = utf8_bin
  COMMENT = '学生';

CREATE TABLE selected_course (
  no    INT PRIMARY KEY AUTO_INCREMENT,
  cno   INT COMMENT '课程编号',
  sno   INT COMMENT '学生编号',
  state INT COMMENT '0：审核未通过，1：审核通过，2: 审核不通过',
  score INT COMMENT '此学生本课程的分数'
)
  AUTO_INCREMENT = 400001
  DEFAULT CHARSET utf8, ENGINE INNODB
  COLLATE = utf8_bin
  COMMENT = '选课';

CREATE TABLE login (
  no       INT PRIMARY KEY
  COMMENT '一共4种角色的编号，教务处和机房有固定的编号',
  password VARCHAR(100),
  role     VARCHAR(50) COMMENT
    '学生：STUDENT，教师：TEACHER，教务：JWC，机房：JF'
)
  DEFAULT CHARSET utf8, ENGINE INNODB
  COLLATE = utf8_bin
  COMMENT = '登录账号';