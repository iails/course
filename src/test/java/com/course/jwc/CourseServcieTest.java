package com.course.jwc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.course.service.CourseService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServcieTest {
	@Autowired
	CourseService cService;

	@Test
	public void test() {
		assert cService.findApproved().size() == 0;
	}
}
