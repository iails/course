package com.course.repository;

import com.course.entity.SelectedCourse;
import com.course.entity.SelectedCourseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SelectedCourseRepository {
    long countByExample(SelectedCourseExample example);

    int deleteByExample(SelectedCourseExample example);

    int deleteByPrimaryKey(Integer no);

    int insert(SelectedCourse record);

    int insertSelective(SelectedCourse record);

    List<SelectedCourse> selectByExample(SelectedCourseExample example);

    SelectedCourse selectByPrimaryKey(Integer no);

    int updateByExampleSelective(@Param("record") SelectedCourse record, @Param("example") SelectedCourseExample example);

    int updateByExample(@Param("record") SelectedCourse record, @Param("example") SelectedCourseExample example);

    int updateByPrimaryKeySelective(SelectedCourse record);

    int updateByPrimaryKey(SelectedCourse record);
}