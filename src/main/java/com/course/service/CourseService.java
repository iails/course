package com.course.service;

import java.util.List;

import com.course.entity.Course;
import com.course.entity.dto.CourseInfo;

public interface CourseService {
	
	void addExperimentCourse(Course course);
	
	void addNormalCourse(Course course);
	
	void updateCourseState(Integer id, Integer state);
	
	/**
	 * @param lesson 这天第几节课(1，2，3，4)之间
	 */
	void updateTime(Integer id, Integer startWeek, Integer endWeek, Integer dayOfWeek, Integer lesson);
	
	void updateClassroom(Integer id, String classroom);
	
	CourseInfo findById(Integer id);
	
	/**
	 * 已审核通过的所有类型的课程
	 */
	List<CourseInfo> findApproved();

	List<CourseInfo> findApprovedAndNotSelectedNormal(Integer userNo);
	
	/**
	 * 已审核通过的实验课
	 */
	List<CourseInfo> findApprovedExperiment();
	
	List<CourseInfo> findExperimentByState(Integer state);
	
	List<CourseInfo> findNormalByState(Integer state);
	/**
	 * 某位老师所开设的所有实验课程
	 */
	List<CourseInfo> findExperimentByTeacherId(Integer tId);

	/**
	 * 某位老师所开设的所有普通课程
	 */
	List<CourseInfo> findNormalByTeacherId(Integer tId);
	
	
	/**
	 * 未审核的所有类型的课程
	 */
	List<CourseInfo> findUnapproved();
	
	/**
	 * 未审核的实验课程
	 */
	List<CourseInfo> findUnapprovedExperiment();
	
}

