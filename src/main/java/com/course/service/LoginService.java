package com.course.service;

import com.course.entity.dto.LoginInfo;

public interface LoginService {
	
	LoginInfo verify(Integer username, String password);
	
}
