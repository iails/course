package com.course.service;

import java.util.List;

import com.course.entity.Course;
import com.course.entity.SelectedCourse;
import com.course.entity.dto.SelectedCourseInfo;

public interface SelectedCourseService {
	
	/**
	 * 老师为本课程全部学生录入了成绩则认为已结课，此时需要修改course的state为3
	 */
	void updateScore(Integer id, Integer score);
	
	void updateState(Integer id, Integer state);
	
	/**
	 * 某位学生已结课的课程成绩
	 */
	List<SelectedCourseInfo> findStudentScore(Integer stuId);
	
	/**
	 * 查询某门课程所有的选课信息(包含成绩)
	 */
	List<SelectedCourseInfo> findByTeacher(Integer tid);
	
	
	void add(SelectedCourse sc);

	/**
	 * 还未审核的
	 */
	List<SelectedCourseInfo> findUnapproved();
	
}
