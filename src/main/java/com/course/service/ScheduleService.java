package com.course.service;

import java.util.List;

import com.course.entity.dto.ScheduleItem;

/**
 * 课程表
 */
public interface ScheduleService {
	
	/**
	 * 不包括审核未通过的选课
	 */
	List<ScheduleItem> getSchedule(Integer stuId);
	
}
