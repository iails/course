package com.course.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.entity.Course;
import com.course.entity.SelectedCourse;
import com.course.entity.SelectedCourseExample;
import com.course.entity.Teacher;
import com.course.entity.TeacherExample;
import com.course.entity.dto.ScheduleItem;
import com.course.repository.CourseRepository;
import com.course.repository.SelectedCourseRepository;
import com.course.repository.TeacherRepository;
import com.course.service.ScheduleService;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	CourseRepository courseRepo;
	
	@Autowired
	SelectedCourseRepository scRepo;
	
	@Autowired
	TeacherRepository teacherRepo;
	
	@Override
	public List<ScheduleItem> getSchedule(Integer stuId) {
		List<ScheduleItem> result =  new ArrayList<>();
		SelectedCourseExample e = new SelectedCourseExample();
		e.createCriteria().andSnoEqualTo(stuId).andStateEqualTo(1);
		List<SelectedCourse> list = scRepo.selectByExample(e);
		for (SelectedCourse sc : list) {
			Integer cno = sc.getCno();
			Course course = courseRepo.selectByPrimaryKey(cno);
			Teacher teacher = teacherRepo.selectByPrimaryKey(course.getTno());
			ScheduleItem scheduleItem = new ScheduleItem();
			
			scheduleItem.setTeacherName(teacher.getName());
			scheduleItem.setCourseName(course.getName());
			scheduleItem.setClassroom(course.getClassroom());
			scheduleItem.setLesson(course.getLesson());
			result.add(scheduleItem);
		}
		return result;
	}

}
