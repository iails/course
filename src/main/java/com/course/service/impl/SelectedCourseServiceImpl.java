package com.course.service.impl;

import static org.hamcrest.CoreMatchers.nullValue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.entity.Course;
import com.course.entity.SelectedCourse;
import com.course.entity.SelectedCourseExample;
import com.course.entity.Student;
import com.course.entity.Teacher;
import com.course.entity.dto.SelectedCourseInfo;
import com.course.repository.CourseRepository;
import com.course.repository.SelectedCourseRepository;
import com.course.repository.StudentRepository;
import com.course.repository.TeacherRepository;
import com.course.service.SelectedCourseService;

@Service
public class SelectedCourseServiceImpl implements SelectedCourseService {
	@Autowired
	SelectedCourseRepository selectedCourseRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	TeacherRepository teacherRepository;

	@Override
	public void updateScore(Integer id, Integer score) {
		SelectedCourse sc = selectedCourseRepository.selectByPrimaryKey(id);
		if (sc != null) {
			sc.setScore(score);
			selectedCourseRepository.updateByPrimaryKeySelective(sc);
		}
	}

	@Override
	public void updateState(Integer id, Integer state) {
		SelectedCourse sc = selectedCourseRepository.selectByPrimaryKey(id);
		if (sc != null) {
			sc.setState(state);
			selectedCourseRepository.updateByPrimaryKeySelective(sc);
		}

	}

	@Override
	public List<SelectedCourseInfo> findStudentScore(Integer stuId) {
		SelectedCourseExample e = new SelectedCourseExample();
		e.createCriteria().andSnoEqualTo(stuId).andStateEqualTo(1);
		List<SelectedCourse> list = selectedCourseRepository.selectByExample(e);
		return toSelectedCourseInfos(list);
	}

	@Override
	public List<SelectedCourseInfo> findByTeacher(Integer tid) {
		List<SelectedCourse> list = selectedCourseRepository.selectByExample(null);
		List<Course> courseList = courseRepository.selectByExample(null);
		List<SelectedCourse> result = list.stream()
				.filter(sc -> courseList.stream().anyMatch(c -> c.getNo().equals(sc.getCno())))
				.collect(Collectors.toList());
		return toSelectedCourseInfos(result);
	}

	@Override
	public void add(SelectedCourse sc) {
		sc.setState(0);
		selectedCourseRepository.insert(sc);
	}

	@Override
	public List<SelectedCourseInfo> findUnapproved() {
		SelectedCourseExample e = new SelectedCourseExample();
		e.createCriteria().andStateEqualTo(0);
		List<SelectedCourse> list = selectedCourseRepository.selectByExample(e);
		return toSelectedCourseInfos(list);
	}

	private List<SelectedCourseInfo> toSelectedCourseInfos(List<SelectedCourse> list) {
		List<SelectedCourseInfo> result = new ArrayList<>();
		for (SelectedCourse selectedCourse : list) {
			Course course = courseRepository.selectByPrimaryKey(selectedCourse.getCno());
			Teacher teacher = teacherRepository.selectByPrimaryKey(course.getTno());
			Student student = studentRepository.selectByPrimaryKey(selectedCourse.getSno());
			SelectedCourseInfo sci = new SelectedCourseInfo();
			sci.setCourse(course.getName());
			sci.setStu(student.getName());
			sci.setTeacher(teacher.getName());
			sci.setSelectedCourse(selectedCourse);
			result.add(sci);
		}
		return result;
	}

}
