package com.course.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.course.entity.Login;
import com.course.entity.Student;
import com.course.entity.Teacher;
import com.course.entity.dto.LoginInfo;
import com.course.repository.LoginRepository;
import com.course.repository.StudentRepository;
import com.course.repository.TeacherRepository;
import com.course.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	TeacherRepository teacherRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Override
	public LoginInfo verify(Integer username, String password) {
		Login login = loginRepository.selectByPrimaryKey(username);
		if (login.getPassword().equals(password)) {
			LoginInfo loginInfo = new LoginInfo();
			loginInfo.setLogin(login);
			if (login.getRole().equals("TEACHER")) {
				Teacher teacher = teacherRepository.selectByPrimaryKey(login.getNo());
				loginInfo.setName(teacher.getName());				
			}else if(login.getRole().equals("STUDENT")) {
				Student student = studentRepository.selectByPrimaryKey(login.getNo());
				loginInfo.setName(student.getName());
			}else if(login.getRole().equals("JWC")) {
				loginInfo.setName("JWC");
			}else {
				loginInfo.setName("JF");
			}
			return loginInfo;			
		}
		return null;
	}

}
