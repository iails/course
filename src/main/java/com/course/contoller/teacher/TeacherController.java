package com.course.contoller.teacher;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.course.entity.Course;
import com.course.entity.dto.CourseInfo;
import com.course.entity.dto.LoginInfo;
import com.course.entity.dto.SelectedCourseInfo;
import com.course.service.CourseService;
import com.course.service.SelectedCourseService;
import com.course.util.LoginUtil;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
	@Autowired
	SelectedCourseService scService;

	@Autowired
	CourseService courseService;

	@GetMapping("/submit-course-information")
	public String SubmitCourseInformation(HttpSession session, Model model) {
		/* 获取到登录老师的id,只显示这个老师提交的课程信息 */
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		/* 把教师的课程信息取出来 */
		List<CourseInfo> list = courseService.findNormalByTeacherId(
				loginInfo.getLogin().getNo());
		model.addAttribute("list", list);
		/* 只返回到本页面 */
		return "teacher/submit-course-information";
	}

	@GetMapping("/grade-input")
	public String GradeInput(HttpSession session,Model model) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		List<SelectedCourseInfo> list = scService.findByTeacher(loginInfo.getLogin().getNo());
		model.addAttribute("list", list);
		return "teacher/grade-input";
	}

	@GetMapping("/experiment-application")
	public String ExperimentApplication(HttpSession session, Model model) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		List<CourseInfo> list = courseService.findExperimentByTeacherId(loginInfo.getLogin().getNo());
		model.addAttribute("list", list);
		return "teacher/experiment-application";
	}

	@PostMapping("/submit-course")
	public String submitCourse(String coursename, HttpSession session) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		Course c = new Course();
		c.setName(coursename);
		c.setTno(loginInfo.getLogin().getNo());
		courseService.addNormalCourse(c);
		return "redirect:submit-course-information";
	}
	@PostMapping("/submit-experiment")
	public String submitExperiment(HttpSession session, String course) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		Course c = new Course();
		c.setName(course);
		c.setTno(loginInfo.getLogin().getNo());
		courseService.addExperimentCourse(c);
		return "redirect:experiment-application";
	}
	
	@PostMapping("/grade")
	public String  grade(Integer course,Integer score) {
		scService.updateScore(course, score);
		return "redirect:grade-input";	
	}
	

	@GetMapping("/test")
	public String Test() {
		return "teacher/test";
	}

}
