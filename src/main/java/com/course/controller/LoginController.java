package com.course.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.course.entity.dto.LoginInfo;
import com.course.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@GetMapping("/login")
	public String loginForm() {
		return "login";
	}

	@PostMapping("/login")
	public String login(HttpSession session, Integer username, String password) {
		LoginInfo loginInfo = loginService.verify(username, password);
		if (loginInfo == null) {
			return "redirect:login";
		}
		session.setAttribute("login", loginInfo);
		return "redirect:";
	}

	@GetMapping("/logout")
	public String login(HttpSession session) {
		session.removeAttribute("login");
		return "redirect:";

	}
}
