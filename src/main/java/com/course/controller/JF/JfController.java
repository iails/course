package com.course.controller.JF;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.course.service.CourseService;

@Controller
@RequestMapping("/jf")
public class JfController {
	@Autowired
	CourseService courseService;

	@GetMapping("/information-management")
	public String InformationManagement(Model model) {
		model.addAttribute("list", courseService.findUnapprovedExperiment());
		return "jf/information-management";
	}
	@GetMapping("/infor-change")
	public String InforChange(Integer course,Integer state) {
		courseService.updateCourseState(course, state);
		return "redirect:information-management";
	}

	@GetMapping("/time-arrangement")
	public String TimeArrangement(Model model) {
		model.addAttribute("list", courseService.findApprovedExperiment());
		return "jf/time-arrangement";
	}

	@GetMapping("/location-arrangement")
	public String LocationArrangement(Model model) {
		model.addAttribute("list", courseService.findApprovedExperiment());
		return "jf/location-arrangement";
	}

	@PostMapping("/arrange-location")
	public String ArrageLocation(Integer course, String classroom) {
		courseService.updateClassroom(course, classroom);
		return "redirect:location-arrangement";
	}

	@PostMapping("/time-arrange")
	public String ArrangeTime(Integer course, Integer startweek, Integer dayofweek, Integer endweek, Integer lesson) {
		courseService.updateTime(course, startweek, endweek, dayofweek, lesson);
		return "redirect:time-arrangement";
	}
}
