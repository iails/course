package com.course.controller.stu;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.course.entity.SelectedCourse;
import com.course.entity.dto.LoginInfo;
import com.course.entity.dto.ScheduleItem;
import com.course.entity.dto.SelectedCourseInfo;
import com.course.service.CourseService;
import com.course.service.ScheduleService;
import com.course.service.SelectedCourseService;
import com.course.util.LoginUtil;

@Controller
@RequestMapping("/stu")
public class StuController {

	@Autowired
	CourseService courseService;

	@Autowired
	ScheduleService scheduleService;

	@Autowired
	SelectedCourseService selectedCourseService;

	@GetMapping("/select-course")
	public String SelectCourse(Model model, HttpSession session) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		model.addAttribute("list", courseService.findApprovedAndNotSelectedNormal(loginInfo.getLogin().getNo()));
		return "stu/select-course";
	}

	@GetMapping("/select/{cno}")
	public String Select(@PathVariable Integer cno, HttpSession session) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		SelectedCourse sc = new SelectedCourse();
		sc.setSno(loginInfo.getLogin().getNo());
		sc.setCno(cno);
		selectedCourseService.add(sc);
		return "redirect:../select-course";
	}

	@GetMapping("/query-course")
	public String QueryCourse(Model model, HttpSession session) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		List<ScheduleItem> list = scheduleService.getSchedule(loginInfo.getLogin().getNo());
		model.addAttribute("list", list);
		return "stu/query-course";
	}

	@GetMapping("/enquiry-grade")
	public String EnquiryGrade(Model model, HttpSession session) {
		LoginInfo loginInfo = LoginUtil.getLoginInfo(session);
		List<SelectedCourseInfo> list = selectedCourseService.findStudentScore(loginInfo.getLogin().getNo());
		model.addAttribute("list", list);
		return "stu/enquiry-grade";
	}

	@GetMapping("/test")
	public String test() {
		return "stu/test";
	}
}
