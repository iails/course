package com.course.controller.jwc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.course.service.CourseService;
import com.course.service.SelectedCourseService;

@Controller
@RequestMapping("/jwc")
public class JwcController {

	@Autowired
	CourseService courseService;

	@Autowired
	SelectedCourseService scService;

	@GetMapping("/review-open-course")
	public String reviewOpenCourse(Model model) {
		model.addAttribute("list", courseService.findUnapproved());
		return "jwc/review-open-course";
	}

	@GetMapping("/review-course")
	public String reviewCourse(Integer course, Integer state) {
		courseService.updateCourseState(course, state);
		return "redirect:review-open-course";
	}

	@GetMapping("/review-select-course")
	public String reviewSelectCourse(Model model) {
		model.addAttribute("list", scService.findUnapproved());
		return "jwc/review-select-course";
	}

	@GetMapping("/review-sc")
	public String reviewSc(Integer sc, Integer state) {
		scService.updateState(sc, state);
		return "redirect:review-select-course";
	}

	@GetMapping("/arrange-teaching-time")
	public String arrangeTeachingTime(Model model) {
		model.addAttribute("list", courseService.findApproved());
		return "jwc/arrange-teaching-time";
	}

	@PostMapping("/arrange-time")
	public String arrangeTime(Integer course, Integer startweek, Integer dayofweek, Integer endweek,
			Integer lesson) {
		courseService.updateTime(course, startweek, endweek, dayofweek, lesson);
		return "redirect:arrange-teaching-time";
	}

	@GetMapping("/arrange-teaching-loc")
	public String arrangeTeachingLocation(Model model) {
		model.addAttribute("list", courseService.findApproved());
		return "jwc/arrange-teaching-loc";
	}

	@PostMapping("/arrange-loc")
	public String arrangeLoc(Integer course, String classroom) {
		courseService.updateClassroom(course, classroom);
		return "redirect:arrange-teaching-loc";
	}

}
