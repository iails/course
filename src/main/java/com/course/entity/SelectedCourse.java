package com.course.entity;

import java.io.Serializable;

/**
 * selected_course
 * @author 
 */
public class SelectedCourse implements Serializable {
    private Integer no;

    /**
     * 课程编号
     */
    private Integer cno;

    /**
     * 学生编号
     */
    private Integer sno;

    /**
     * 0：审核未通过，1：审核通过，2: 审核不通过
     */
    private Integer state;

    /**
     * 此学生本课程的分数
     */
    private Integer score;

    private static final long serialVersionUID = 1L;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public Integer getCno() {
        return cno;
    }

    public void setCno(Integer cno) {
        this.cno = cno;
    }

    public Integer getSno() {
        return sno;
    }

    public void setSno(Integer sno) {
        this.sno = sno;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}