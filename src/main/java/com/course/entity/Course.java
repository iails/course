package com.course.entity;

import java.io.Serializable;

/**
 * course
 * @author 
 */
public class Course implements Serializable {
    /**
     * 课程编号
     */
    private Integer no;

    /**
     * 课程名称
     */
    private String name;

    /**
     * 开课教师编号
     */
    private Integer tno;

    /**
     * 第几周第一节课
     */
    private Integer startweek;

    /**
     * 第几周最后一节课
     */
    private Integer endweek;

    /**
     * 星期几，每周都只有一节课
     */
    private Integer dayofweek;

    /**
     * 一天中的第几节, 1代表上午第1节课，
                        4代表下午最后一节课，5代表晚上
     */
    private Integer lesson;

    /**
     * 教室名称，如D101
     */
    private String classroom;

    /**
     * 0: 非实验课，1：实验课
     */
    private Integer type;

    /**
     * 0：还未通过审核，1：审核通过，2：审核不通过，
                         3：已结课
     */
    private Integer state;

    private static final long serialVersionUID = 1L;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTno() {
        return tno;
    }

    public void setTno(Integer tno) {
        this.tno = tno;
    }

    public Integer getStartweek() {
        return startweek;
    }

    public void setStartweek(Integer startweek) {
        this.startweek = startweek;
    }

    public Integer getEndweek() {
        return endweek;
    }

    public void setEndweek(Integer endweek) {
        this.endweek = endweek;
    }

    public Integer getDayofweek() {
        return dayofweek;
    }

    public void setDayofweek(Integer dayofweek) {
        this.dayofweek = dayofweek;
    }

    public Integer getLesson() {
        return lesson;
    }

    public void setLesson(Integer lesson) {
        this.lesson = lesson;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}