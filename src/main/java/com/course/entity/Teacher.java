package com.course.entity;

import java.io.Serializable;

/**
 * teacher
 * @author 
 */
public class Teacher implements Serializable {
    /**
     * 教师编号
     */
    private Integer no;

    /**
     * 教师名称
     */
    private String name;

    /**
     * 教师年龄
     */
    private Integer age;

    /**
     * 教师性别
     */
    private String gender;

    /**
     * 教师电话
     */
    private String phone;

    /**
     * 教师办公位置
     */
    private String office;

    private static final long serialVersionUID = 1L;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}