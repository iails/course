package com.course.entity;

import java.util.ArrayList;
import java.util.List;

public class CourseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public CourseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNoIsNull() {
            addCriterion("`no` is null");
            return (Criteria) this;
        }

        public Criteria andNoIsNotNull() {
            addCriterion("`no` is not null");
            return (Criteria) this;
        }

        public Criteria andNoEqualTo(Integer value) {
            addCriterion("`no` =", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotEqualTo(Integer value) {
            addCriterion("`no` <>", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoGreaterThan(Integer value) {
            addCriterion("`no` >", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("`no` >=", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoLessThan(Integer value) {
            addCriterion("`no` <", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoLessThanOrEqualTo(Integer value) {
            addCriterion("`no` <=", value, "no");
            return (Criteria) this;
        }

        public Criteria andNoIn(List<Integer> values) {
            addCriterion("`no` in", values, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotIn(List<Integer> values) {
            addCriterion("`no` not in", values, "no");
            return (Criteria) this;
        }

        public Criteria andNoBetween(Integer value1, Integer value2) {
            addCriterion("`no` between", value1, value2, "no");
            return (Criteria) this;
        }

        public Criteria andNoNotBetween(Integer value1, Integer value2) {
            addCriterion("`no` not between", value1, value2, "no");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("`name` is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("`name` is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("`name` =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("`name` <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("`name` >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("`name` >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("`name` <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("`name` <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("`name` like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("`name` not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("`name` in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("`name` not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("`name` between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("`name` not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andTnoIsNull() {
            addCriterion("tno is null");
            return (Criteria) this;
        }

        public Criteria andTnoIsNotNull() {
            addCriterion("tno is not null");
            return (Criteria) this;
        }

        public Criteria andTnoEqualTo(Integer value) {
            addCriterion("tno =", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoNotEqualTo(Integer value) {
            addCriterion("tno <>", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoGreaterThan(Integer value) {
            addCriterion("tno >", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("tno >=", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoLessThan(Integer value) {
            addCriterion("tno <", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoLessThanOrEqualTo(Integer value) {
            addCriterion("tno <=", value, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoIn(List<Integer> values) {
            addCriterion("tno in", values, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoNotIn(List<Integer> values) {
            addCriterion("tno not in", values, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoBetween(Integer value1, Integer value2) {
            addCriterion("tno between", value1, value2, "tno");
            return (Criteria) this;
        }

        public Criteria andTnoNotBetween(Integer value1, Integer value2) {
            addCriterion("tno not between", value1, value2, "tno");
            return (Criteria) this;
        }

        public Criteria andStartweekIsNull() {
            addCriterion("startweek is null");
            return (Criteria) this;
        }

        public Criteria andStartweekIsNotNull() {
            addCriterion("startweek is not null");
            return (Criteria) this;
        }

        public Criteria andStartweekEqualTo(Integer value) {
            addCriterion("startweek =", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekNotEqualTo(Integer value) {
            addCriterion("startweek <>", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekGreaterThan(Integer value) {
            addCriterion("startweek >", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekGreaterThanOrEqualTo(Integer value) {
            addCriterion("startweek >=", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekLessThan(Integer value) {
            addCriterion("startweek <", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekLessThanOrEqualTo(Integer value) {
            addCriterion("startweek <=", value, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekIn(List<Integer> values) {
            addCriterion("startweek in", values, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekNotIn(List<Integer> values) {
            addCriterion("startweek not in", values, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekBetween(Integer value1, Integer value2) {
            addCriterion("startweek between", value1, value2, "startweek");
            return (Criteria) this;
        }

        public Criteria andStartweekNotBetween(Integer value1, Integer value2) {
            addCriterion("startweek not between", value1, value2, "startweek");
            return (Criteria) this;
        }

        public Criteria andEndweekIsNull() {
            addCriterion("endweek is null");
            return (Criteria) this;
        }

        public Criteria andEndweekIsNotNull() {
            addCriterion("endweek is not null");
            return (Criteria) this;
        }

        public Criteria andEndweekEqualTo(Integer value) {
            addCriterion("endweek =", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekNotEqualTo(Integer value) {
            addCriterion("endweek <>", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekGreaterThan(Integer value) {
            addCriterion("endweek >", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekGreaterThanOrEqualTo(Integer value) {
            addCriterion("endweek >=", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekLessThan(Integer value) {
            addCriterion("endweek <", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekLessThanOrEqualTo(Integer value) {
            addCriterion("endweek <=", value, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekIn(List<Integer> values) {
            addCriterion("endweek in", values, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekNotIn(List<Integer> values) {
            addCriterion("endweek not in", values, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekBetween(Integer value1, Integer value2) {
            addCriterion("endweek between", value1, value2, "endweek");
            return (Criteria) this;
        }

        public Criteria andEndweekNotBetween(Integer value1, Integer value2) {
            addCriterion("endweek not between", value1, value2, "endweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekIsNull() {
            addCriterion("`dayofweek` is null");
            return (Criteria) this;
        }

        public Criteria andDayofweekIsNotNull() {
            addCriterion("`dayofweek` is not null");
            return (Criteria) this;
        }

        public Criteria andDayofweekEqualTo(Integer value) {
            addCriterion("`dayofweek` =", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekNotEqualTo(Integer value) {
            addCriterion("`dayofweek` <>", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekGreaterThan(Integer value) {
            addCriterion("`dayofweek` >", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekGreaterThanOrEqualTo(Integer value) {
            addCriterion("`dayofweek` >=", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekLessThan(Integer value) {
            addCriterion("`dayofweek` <", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekLessThanOrEqualTo(Integer value) {
            addCriterion("`dayofweek` <=", value, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekIn(List<Integer> values) {
            addCriterion("`dayofweek` in", values, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekNotIn(List<Integer> values) {
            addCriterion("`dayofweek` not in", values, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekBetween(Integer value1, Integer value2) {
            addCriterion("`dayofweek` between", value1, value2, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andDayofweekNotBetween(Integer value1, Integer value2) {
            addCriterion("`dayofweek` not between", value1, value2, "dayofweek");
            return (Criteria) this;
        }

        public Criteria andLessonIsNull() {
            addCriterion("lesson is null");
            return (Criteria) this;
        }

        public Criteria andLessonIsNotNull() {
            addCriterion("lesson is not null");
            return (Criteria) this;
        }

        public Criteria andLessonEqualTo(Integer value) {
            addCriterion("lesson =", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonNotEqualTo(Integer value) {
            addCriterion("lesson <>", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonGreaterThan(Integer value) {
            addCriterion("lesson >", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonGreaterThanOrEqualTo(Integer value) {
            addCriterion("lesson >=", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonLessThan(Integer value) {
            addCriterion("lesson <", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonLessThanOrEqualTo(Integer value) {
            addCriterion("lesson <=", value, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonIn(List<Integer> values) {
            addCriterion("lesson in", values, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonNotIn(List<Integer> values) {
            addCriterion("lesson not in", values, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonBetween(Integer value1, Integer value2) {
            addCriterion("lesson between", value1, value2, "lesson");
            return (Criteria) this;
        }

        public Criteria andLessonNotBetween(Integer value1, Integer value2) {
            addCriterion("lesson not between", value1, value2, "lesson");
            return (Criteria) this;
        }

        public Criteria andClassroomIsNull() {
            addCriterion("classroom is null");
            return (Criteria) this;
        }

        public Criteria andClassroomIsNotNull() {
            addCriterion("classroom is not null");
            return (Criteria) this;
        }

        public Criteria andClassroomEqualTo(String value) {
            addCriterion("classroom =", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomNotEqualTo(String value) {
            addCriterion("classroom <>", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomGreaterThan(String value) {
            addCriterion("classroom >", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomGreaterThanOrEqualTo(String value) {
            addCriterion("classroom >=", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomLessThan(String value) {
            addCriterion("classroom <", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomLessThanOrEqualTo(String value) {
            addCriterion("classroom <=", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomLike(String value) {
            addCriterion("classroom like", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomNotLike(String value) {
            addCriterion("classroom not like", value, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomIn(List<String> values) {
            addCriterion("classroom in", values, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomNotIn(List<String> values) {
            addCriterion("classroom not in", values, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomBetween(String value1, String value2) {
            addCriterion("classroom between", value1, value2, "classroom");
            return (Criteria) this;
        }

        public Criteria andClassroomNotBetween(String value1, String value2) {
            addCriterion("classroom not between", value1, value2, "classroom");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("`type` is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("`type` is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("`type` =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("`type` <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("`type` >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("`type` >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("`type` <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("`type` <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("`type` in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("`type` not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("`type` between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("`type` not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("`state` is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("`state` is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("`state` =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("`state` <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("`state` >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("`state` >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("`state` <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("`state` <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("`state` in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("`state` not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("`state` between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("`state` not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}