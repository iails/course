package com.course.entity.dto;

import com.course.entity.Login;

public class LoginInfo {
	private Login login;
	
	/**
	 * 账号所有者的名字
	 */
	private String name;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
