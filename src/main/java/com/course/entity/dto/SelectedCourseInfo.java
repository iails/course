package com.course.entity.dto;

import com.course.entity.SelectedCourse;

/**
 * 选课信息详情
 */
public class SelectedCourseInfo {
	private SelectedCourse selectedCourse;

	/**
	 * 學生名字
	 */
	private String stu;
	
	/**
	 * 课程名称
	 */
	private String course;
	
	/**
	 * 开课教师名字
	 */
	private String teacher;

	public SelectedCourse getSelectedCourse() {
		return selectedCourse;
	}

	public void setSelectedCourse(SelectedCourse selectedCourse) {
		this.selectedCourse = selectedCourse;
	}

	public String getStu() {
		return stu;
	}

	public void setStu(String stu) {
		this.stu = stu;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
	
	
	
}
