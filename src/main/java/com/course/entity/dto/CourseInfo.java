package com.course.entity.dto;

import com.course.entity.Course;

public class CourseInfo {
	private Course course;
	
	/**
	 * 开课教师名字
	 */
	private String teacher;

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
	
}
