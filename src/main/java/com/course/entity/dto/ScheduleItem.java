package com.course.entity.dto;

/**
 * 课程表项
 *
 */
public class ScheduleItem {
	/**
	 * 第几节
	 */
	private Integer lesson;
	
	private String courseName;
	
	private String teacherName;
	
	private String classroom;

	public Integer getLesson() {
		return lesson;
	}

	public void setLesson(Integer lesson) {
		this.lesson = lesson;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getClassroom() {
		return classroom;
	}

	public void setClassroom(String classroom) {
		this.classroom = classroom;
	}
	
	
}
