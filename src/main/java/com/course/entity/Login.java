package com.course.entity;

import java.io.Serializable;

/**
 * login
 * @author 
 */
public class Login implements Serializable {
    /**
     * 一共4种角色的编号，教务处和机房有固定的编号
     */
    private Integer no;

    private String password;

    /**
     * 学生：STUDENT，教师：TEACHER，教务：JWC，机房：JF
     */
    private String role;

    private static final long serialVersionUID = 1L;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}