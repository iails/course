package com.course.entity;

import java.io.Serializable;

/**
 * student
 * @author 
 */
public class Student implements Serializable {
    /**
     * 学生编号
     */
    private Integer no;

    /**
     * 学生名称
     */
    private String name;

    /**
     * 教师年龄
     */
    private Integer age;

    /**
     * 教师性别
     */
    private String gender;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 班级名称
     */
    private String classname;

    /**
     * 宿舍
     */
    private String apartment;

    private static final long serialVersionUID = 1L;

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }
}