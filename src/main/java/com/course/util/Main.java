package com.course.util;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by iails on 2019-06-09
 */
public class Main {

    private static Supplier<String> nameSupplier = RandomValue::getChineseName;
    private static Supplier<String> phoneSupplier = () -> RandomValue.getTel().substring(0, 6);
    private static Supplier<String> locSupplier = RandomValue::getRoad;
    private static List<String> courseList = Arrays.asList("高等数学A", "高等数学B", "高等数学C", "高等数学D", "高等数学E", "高等数学F", "高等数学G");
    
    private static void generate() throws IOException {
    	DataGenerator teacher = new DataGenerator("teacher")
                .with("no", new DataGenerator.SequenceSupplier(200001))
                .with("name", nameSupplier)
                .with("age", 26, 55)
                .with("gender", Arrays.asList("男", "女", "未知"))
                .with("phone", phoneSupplier)
                .with("office", locSupplier)
                .generate(50);
        DataGenerator student = new DataGenerator("student")
                .with("no", new DataGenerator.SequenceSupplier(300001))
                .with("name", nameSupplier)
                .with("age", 16, 25)
                .with("gender", Arrays.asList("男", "女", "未知"))
                .with("phone", phoneSupplier)
                .with("classname", phoneSupplier)
                .with("apartment", locSupplier)
                .generate(50);
        DataGenerator course = new DataGenerator("course")
                .with("no", new DataGenerator.SequenceSupplier(100001))
                .with("name", courseList)
                .with("tno", teacher.getColumn("no"))
                .with("startweek", 1, 12)
                .with("endweek", 10, 18)
                .with("dayofweek", 1, 7)
                .with("lesson", 1, 4)
                .with("classroom", Arrays.asList("A", "B", "C", "D", "E", "F"), Arrays.asList("101", "203", "302", "402", "502"))
                .with("type", 0, 1)
                .with("state", 0, 3)
                .generate(50);
        DataGenerator selectedCourse = new DataGenerator("selected_course")
                .with("no", new DataGenerator.SequenceSupplier(400001))
                .with("cno", course.getColumn("no"))
                .with("sno", student.getColumn("no"))
                .with("state", 0, 1)
                .with("score", 50, 99)
                .generate(50);
        DataGenerator stuLogin = new DataGenerator("login")
                .with("no", new DataGenerator.SequenceSupplier(300001))
                .with("password", 101, 199)
                .with("role", () -> "STUDENT")
                .generate(50);
        DataGenerator teacherLogin = new DataGenerator("login")
                .with("no", new DataGenerator.SequenceSupplier(200001))
                .with("password", 101, 199)
                .with("role", () -> "TEACHER")
                .generate(50);
        DataGenerator jwcLogin = new DataGenerator("login")
                .with("no", new DataGenerator.SequenceSupplier(123))
                .with("password", 123, 123)
                .with("role", () -> "JWC")
                .generate(1);
        DataGenerator jfLogin = new DataGenerator("login")
                .with("no", new DataGenerator.SequenceSupplier(321))
                .with("password", 321, 321)
                .with("role", () -> "JF")
                .generate(1);
        StringBuilder result = new StringBuilder();
        result.append(course.getSql())
                .append(teacher.getSql())
                .append(student.getSql())
                .append(selectedCourse.getSql())
                .append(stuLogin.getSql())
                .append(teacherLogin.getSql())
                .append(jwcLogin.getSql())
                .append(jfLogin.getSql());
        Files.write(Paths.get("data.sql"), result.toString().getBytes());
    }
    
//    public static void main(String[] args) throws IOException {
//        
//    }
}
