package com.course.util;

import javax.servlet.http.HttpSession;

import com.course.entity.dto.LoginInfo;

public class LoginUtil {
	public static LoginInfo getLoginInfo(HttpSession session) {
		LoginInfo loginInfo = (LoginInfo) session.getAttribute("login");
		return loginInfo;
	}
}
