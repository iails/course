package com.course.exception;

public class TipException extends RuntimeException {

	
	public TipException(String message) {
		super(message);
	}
}
